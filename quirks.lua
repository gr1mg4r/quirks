local action = require "necro.game.system.Action"
local aggro = require "necro.game.enemy.Aggro"
local attack = require "necro.game.character.Attack"
local beatmap = require "necro.audio.Beatmap"
local boss = require "necro.game.level.Boss"
local collision = require "necro.game.tile.Collision"
local components = require "necro.game.data.Components"
local currentLevel = require "necro.game.level.CurrentLevel"
local customEntities = require "necro.game.data.CustomEntities"
local damage = require "necro.game.system.Damage"
local event = require "necro.event.Event"
local move = require "necro.game.system.Move"
local objectEvents = require "necro.game.object.ObjectEvents"
local settings = require "necro.config.Settings"
local spellTargeting = require "necro.game.spell.SpellTargeting"
local turn = require "necro.cycles.Turn"
local vision = require "necro.game.vision.Vision"

local ecs = require "system.game.Entities"
local enum = require "system.utils.Enum"

local field = components.field
local constant = components.constant
local dependency = components.dependency


local function quirk(name, default)
	return settings.entitySchema.bool {
		id = name,
		default = default,
	}
end

-------------------------
-- Beetle regeneration --
-------------------------
beetleRegeneration = quirk("beetleRegeneration")

components.register {
	quirks_shieldHealOnHit = {},
}

event.entitySchemaLoadNamedEnemy.add("beetleRegeneration", "beetle", function (ev)
	if beetleRegeneration then
		ev.entity.quirks_shieldHealOnHit = {}
	end
end)

event.objectTakeDamage.add("shieldHeal", {order = "shieldEffect", filter = {"quirks_shieldHealOnHit", "health"}}, function (ev)
	if ev.shielded then
		ev.entity.health.health = ev.entity.health.maxHealth
	end
end)

----------------------
-- Pixie-rapier bug --
----------------------
pixieRapier = quirk("pixieRapier")

components.register {
	quirks_damageRapierHoldersOnHit = {},
}

event.entitySchemaLoadNamedEntity.add("pixieRapier", "Pixie", function (ev)
	if pixieRapier then
		ev.entity.quirks_damageRapierHoldersOnHit = {}
	end
end)

event.objectTakeDamage.add("pixieRapier", {order = "spell", sequence = 1, filter = "quirks_damageRapierHoldersOnHit"}, function (ev)
	if damage.Flag.check(ev.type, damage.Flag.FIRE) and not damage.Flag.check(ev.type, damage.Flag.EXPLOSIVE) then
		return
	end
	for rapier in ecs.entitiesWithComponents {"item", "weaponTypeRapier"} do
		local holder = ecs.getEntityByID(rapier.item.holder)
		if holder and rapier.item.equipped then
			damage.inflict {
				attacker = ev.victim,
				victim = holder,
				damage = 4,
				type = damage.Type.EXPLOSIVE,
			}
		end
	end
end)

---------------------
-- Nightmare aggro --
---------------------
nightmareAggro = quirk("nightmareAggro")

event.frame.add("nightmareAggro", {order = "aggro", sequence = 1}, function (ev)
	if nightmareAggro and ev.preAI then
		for entity in ecs.entitiesWithComponents {"aggro", "visibility", "position"} do
			local x, y = entity.position.x, entity.position.y
			if vision.isShadowed(x, y) and entity.visibility.revealed then
				entity.aggro.active = true
			end
		end
	end
end)

----------------
-- Bomb aggro --
----------------
bombAggro = quirk("bombAggro")

event.entitySchemaLoadNamedEntity.add("bombAggro", "gargoyle3", function (ev)
	ev.entity.explosive = {}
end)

local function bombWillExplode()
	for bomb in ecs.entitiesWithComponents {"explosive", "beatDelay", "character"} do
		if bomb.character.canAct and bomb.beatDelay.counter == 0 then
			return true
		end
	end
end

event.frame.override("aggro", 1, function (func, ev)
	return func { preAI = ev.preAI and not (bombAggro and bombWillExplode()) }
end)

--------------------
-- Courage duping --
--------------------
courageDuping = quirk("courageDuping", true)

event.entitySchemaLoadItem.add("courageDuping", {order = "currency", sequence = 1}, function (ev)
	if ev.flags.isCoin then
		ev.entity.itemStackMergeInWorld.dupable = courageDuping
	end
end)

---------------------------
-- Familiar displacement --
---------------------------
familiarDisplacement = quirk("familiarDisplacement")

components.register {
	quirks_familiarDisplacement = {},
}

event.entitySchemaLoadPlayer.add("familiarDisplacement", {order = "overrides", sequence = 1}, function (ev)
	if familiarDisplacement then
		ev.entity.quirks_familiarDisplacement = {}
	end
end)

event.objectMove.add("saveXY", {order = "damage", sequence = -1, filter = {"quirks_familiarDisplacement", "followed"}}, function (ev)
	ev.oldX, ev.oldY = ev.x, ev.y
end)

event.objectMove.add("familiarDisplacement", {order = "damageLate", sequence = 1, filter = {"quirks_familiarDisplacement", "followed"}}, function (ev)
	local dx, dy = ev.x - ev.oldX, ev.y - ev.oldY
	if dx ~= 0 or dy ~= 0 then
		for _, followerID in ipairs(ev.entity.followed.followers) do
			local follower = ecs.getEntityByID(followerID)
			if follower and follower.follower then
				follower.follower.dx = follower.follower.dx + dx
				follower.follower.dy = follower.follower.dy + dy
				move.relative(follower, dx, dy, bit.band(ev.moveType, move.Flag.TWEEN))
			end
		end
	end
end)

-----------------
-- Prevpos bug --
-----------------
spellPrevpos = quirk("spellPrevpos")

local function preservesHasMoved(ev)
	return ev.action == action.Special.SPELL_1
		or ev.action == action.Special.SPELL_2
		or (ev.result == nil and beatmap.getForEntity(ev.entity).isRhythmEffectivelyIgnored())
end

event.objectSpecialAction.override("resetHasMoved", 1, function (func, ev)
	if not (spellPrevpos and preservesHasMoved(ev)) then
		ev.entity.hasMoved.value = false
	end
end)

--------------------
-- Conjurer chest --
--------------------
conjurerChest = quirk("conjurerChest")

customEntities.register {
	name = "ConjurerChest",
	position = {},
	collision = { mask = collision.mask(collision.Group.CHARACTER, collision.Type.CRATE) },
	attackable = { flags = attack.Flag.PAIN },
}

event.entitySchemaLoadNamedEnemy.add("conjurerChest", {key = "conjurer", sequence = 1}, function (ev)
	if conjurerChest then
		ev.entity.multiTile = false
		ev.entity.characterWithAttachment = {
			attachmentType = "quirks_ConjurerChest",
			dx = 1,
		}
	end
end)

------------------
-- Axe orb bump --
------------------
axeOrbBump = quirk("axeOrbBump")

event.entitySchemaLoadEntity.add("OrbBump", {order = "weaponPostProcess", sequence = -1}, function (ev)
	if axeOrbBump and (ev.entity.weaponTypeAxe or ev.entity.weaponTypeCat) then
		ev.entity.weaponPattern.pattern.dashCollisionMask = collision.Group.SOLID
	end
end)

-----------------
-- ND1 iframes --
-----------------
nd1Iframes = quirk("nd1Iframes")

event.objectHeal.add("ND1Iframes", {order = "invincibility", sequence = -1}, function (ev)
	if nd1Iframes and currentLevel.getBossType() == boss.Type.NECRODANCER then
		dbg(ev.invincibility)
		ev.invincibility = 0
	end
end)

-----------------------
-- Orthogonalization --
-----------------------
local orthoMode = enum.sequence {
	ALL_OFF = 0,
	DEFAULT = 1,
	ALL_ON = 2,
}

orthogonalize = settings.entitySchema.enum {
	name = "Orthogonalize",
	default = orthoMode.DEFAULT,
	enum = orthoMode,
}

components.register {
	quirks_spellcastWeirdDamageDirection = {},
	quirks_orthogonalizeDamageOnMove = {},
}

event.entitySchemaLoadEntity.add("orthogonalizeExplosions", {order = "namedOverrides", sequence = 1}, function (ev)
	if ev.entity.spellcastInflictDamage and ev.entity.spellcastInflictDamage.type == damage.Type.EXPLOSIVE then
		ev.entity.spellcastInflictDamage.orthogonalize = orthogonalize > orthoMode.ALL_OFF
	end
end)

event.entitySchemaLoadNamedEntity.add("orthogonalizePain", {key = "SpellcastPain", sequence = 1}, function (ev)
	if orthogonalize == orthoMode.ALL_ON then
		ev.entity.spellcastInflictDamage = false
		ev.entity.quirks_spellcastWeirdDamageDirection = {}
	end
end)

event.entitySchemaLoadNamedEntity.add("orthogonalizeLunging", {key = "FeetBootsLunging", sequence = 1}, function (ev)
	if orthogonalize == orthoMode.ALL_ON then
		ev.entity.quirks_orthogonalizeDamageOnMove = {}
	end
end)

event.spellcast.add("inflictSpellDamage", {order = "inflictDamage", filter = "quirks_spellcastWeirdDamageDirection"}, function (ev)
	for target in spellTargeting.attackableTargets(ev, attack.Flag.CHARACTER) do
		local direction = action.move(target.position.x - ev.x, target.position.y - ev.y)
		local rotation = ({0, 1, 0, -1})[(direction - ev.direction) % 4 + 1]

		damage.inflict {
			attacker = ev.caster,
			victim = target,
			damage = 1,
			type = damage.Type.MAGIC,
			direction = (direction + rotation - 1) % 8 + 1,
		}
		-- don't count tombstones
		if not target.excludeFromSpellTargetCount then
			ev.targetCount = ev.targetCount + 1
		end
	end
end)

event.holderMove.add("preDamageOnMove", {order = "damage", sequence = -1, filter = "quirks_orthogonalizeDamageOnMove"}, function (ev)
	local dx, dy = ev.x - ev.prevX, ev.y - ev.prevY
	if dx ~= 0 and dy ~= 0 then
		ev.prevPrevY = ev.prevY
		ev.prevY = ev.y
	end
end)

event.holderMove.add("postDamageOnMove", {order = "damage", sequence = 1, filter = "quirks_orthogonalizeDamageOnMove"}, function (ev)
	if ev.prevPrevY then
		ev.prevY = ev.prevPrevY
		ev.prevPrevY = nil
	end
end)

event.objectInteract.override("takeDamage", {order = "selfDestruct", sequence = 1, filter = "interactableTakeDamage"}, function (func, ev)
	if not ev.suppressed and orthogonalize == orthoMode.ALL_ON and ev.entity.position.x ~= ev.interactor.position.x then
		objectEvents.fire("takeDamage", ev.entity, {
			attacker = ev.interactor,
			victim = ev.entity,
			damage = ev.entity.interactableTakeDamage.damage,
			type = ev.entity.interactableTakeDamage.type,
			direction = action.move(ev.entity.position.x - ev.interactor.position.x, 0),
		})
		ev.result = action.Result.ATTACK
	else
		return func(ev)
	end
end)


event.entitySchemaLoadNamedEntity.add("orthogonalizeExplosions", {order = "namedOverrides", sequence = 1}, function (ev)
	if ev.entity.spellcastInflictDamage and ev.entity.spellcastInflictDamage.type == damage.Type.EXPLOSIVE then
		ev.entity.spellcastInflictDamage.orthogonalize = orthogonalize > orthoMode.ALL_OFF
	end
end)
